package com.dbs.demo.model;

import javax.persistence.*;

/**
 * Entity class which represent persistent entity in the database
 */

@Entity
@Table(name="DBS_ORDER")
public class OrderEntity {


    public OrderEntity() {

    }

    public OrderEntity(Long id, String itemName) {
        this.id = id;
        this.itemName = itemName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="item_name")
    private String itemName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


}
