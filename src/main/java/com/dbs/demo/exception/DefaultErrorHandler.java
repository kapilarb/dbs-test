package com.dbs.demo.exception;


import com.dbs.demo.controller.OrderTransactionController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

/**
 * This error handler is used to capture errors which happens at the MQ listener
 * while processing the messages
 */

public class DefaultErrorHandler implements ErrorHandler {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(OrderTransactionController.class);

    @Override
    public void handleError(Throwable throwable) {

        LOGGER.error(throwable.getCause().getMessage());
    }
}
