package com.dbs.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * This DTO object capture the very basic information of a product that user has selected
 * This DTO is what is expected in the rest controller's request body
 * And Same DTO will be forwarded to dbs.orderQueue
 *
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderDTO {

    private Long id;

    private String itemName;

}
