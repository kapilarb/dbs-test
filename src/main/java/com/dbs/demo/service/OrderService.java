package com.dbs.demo.service;

import com.dbs.demo.model.OrderEntity;
import com.dbs.demo.exception.RecordNotFoundException;

import com.dbs.demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This service interface cater for retrieving products and creting new products
 */

@Service
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
public class OrderService {

    @Autowired
    OrderRepository repository;

    public OrderEntity getOrderById(Long id) throws RecordNotFoundException
    {
        Optional<OrderEntity> order = repository.findById(id);

        if(order.isPresent()) {
            return order.get();
        } else {
            throw new RecordNotFoundException("No order record exist for given id");
        }
    }

    public OrderEntity createOrder(OrderEntity entity) throws RecordNotFoundException
    {

        entity = repository.save(entity);
        return entity;

    }
}
