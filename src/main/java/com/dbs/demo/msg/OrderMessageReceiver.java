package com.dbs.demo.msg;

import com.dbs.demo.exception.OrderProcessingException;
import com.dbs.demo.dto.OrderDTO;
import com.dbs.demo.model.OrderEntity;
import com.dbs.demo.service.OrderService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * This is the MQ listener which receives messages sent from users shopping cart while selecting a product
 *
 */

@Component
public class OrderMessageReceiver {

    @Autowired
    OrderService service;

    /*
    model mapper is used for mapping OrderDTO data to OrderEntity
     */

    @Autowired
    ModelMapper modelMapper;

    private static final Logger LOGGER =
            LoggerFactory.getLogger(OrderMessageReceiver.class);

    @JmsListener(destination = "dbs.orderQueue", containerFactory = "dbsFactory")
    public void receiveQueue(OrderDTO order) throws Exception {

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        ModelMapper modelMapper = new ModelMapper();

        OrderEntity orderEntity = modelMapper.map(order, OrderEntity.class);
        try {
            service.createOrder(orderEntity);
            //throw new Exception();
        } catch (Exception e) {
            LOGGER.error("Error happend while trying to persist order : {} error is : {}",order, e);
            throw new OrderProcessingException("problem occurred while processing the order.");

        }

    }


}