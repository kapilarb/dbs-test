package com.dbs.demo.repository;


import com.dbs.demo.model.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * This repository layer supports basic CRUD operations for OrderEntity
 */

@Repository
@Transactional(rollbackFor = Exception.class)
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
