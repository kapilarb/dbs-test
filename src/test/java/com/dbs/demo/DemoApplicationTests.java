package com.dbs.demo;

import com.dbs.demo.dto.OrderDTO;
import com.dbs.demo.exception.RecordNotFoundException;
import com.dbs.demo.model.OrderEntity;
import com.dbs.demo.repository.OrderRepository;
import com.dbs.demo.service.OrderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired private OrderService orderService;
	@Autowired private DataSource dataSource;
	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private EntityManager entityManager;
	@Autowired private OrderRepository orderRepository;
	@Autowired private JmsMessagingTemplate jmsMessagingTemplate;
	@Autowired ModelMapper modelMapper;
	

	@Test
	public void testSaveOrder() throws RecordNotFoundException {

		OrderEntity orderEntity_created = new OrderEntity(1L, "Test_ORDER");
		orderService.createOrder(orderEntity_created);
		OrderEntity orderEntity_retrived = orderService.getOrderById(1L);
		Assert.assertNotNull(orderEntity_retrived);
		Assert.assertEquals(orderEntity_created.getId(), orderEntity_retrived.getId());
		Assert.assertEquals(orderEntity_created.getItemName(), orderEntity_retrived.getItemName());
	}

	@Test
	public void injectedComponentsAreNotNull(){
		Assert.assertNotNull(dataSource);
		Assert.assertNotNull(jdbcTemplate);
		Assert.assertNotNull(entityManager);
		Assert.assertNotNull(orderRepository);
		Assert.assertNotNull(jmsMessagingTemplate);
		Assert.assertNotNull(modelMapper);
	}

	@Test
	public void testSendingMessage() throws RecordNotFoundException {

		OrderDTO order = new OrderDTO();
		order.setId(1L);
		order.setItemName("Test Item");
		jmsMessagingTemplate.convertAndSend("dbs.orderQueue", order);
		        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		OrderEntity orderEntity_retrived = orderService.getOrderById(1L);
		Assert.assertNotNull(orderEntity_retrived);
		Assert.assertEquals(orderEntity_retrived.getId(), order.getId());

	}

}
